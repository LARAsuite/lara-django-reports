"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_reports models *

:details: lara_django_reports database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

import logging
import datetime
import uuid
from random import randint

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models

from lara_django_base.models import DataType, MediaType, ExtraDataAbstr, Tag


settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
       e.g. more telephone numbers, customer numbers, ... """

    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)  # models.AutoField(primary_key=True)
    file = models.FileField(
        upload_to='base', blank=True, null=True, help_text="rel. path/filename")
