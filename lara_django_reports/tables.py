"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_reports admin *

:details: lara_django_reports admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_reports >> tables.py" to update this file
________________________________________________________________________
"""
